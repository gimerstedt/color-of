import * as jeeves from 'abs/path'
import fruitColors from 'abs/colors'
import { Fruit } from 'abs/fruit'

export const colorOf = (fruit: Fruit) => {
  if (jeeves.isBanana(fruit)) return fruitColors.banana
  else if (jeeves.isOrange(fruit)) return fruitColors.orange
}
