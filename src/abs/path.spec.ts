import * as sut from 'abs/path'

describe('jeeves', () => {
  it('answers questions', () => {
    expect(sut.isBanana('banana')).toBeTruthy()
    expect(sut.isOrange('orange')).toBeTruthy()
    expect(sut.isBanana('orange')).toBeFalsy()
    expect(sut.isOrange('banana')).toBeFalsy()
  })
})
