import { Fruit } from 'abs/fruit'

export const isBanana = (fruit: Fruit) => fruit === 'banana'
export const isOrange = (fruit: Fruit) => fruit === 'orange'
