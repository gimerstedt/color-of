import * as sut from '.'

describe('colorOf', () => {
  it('knows the color of bananas', () => {
    const res = sut.colorOf('banana')
    expect(res).toBe('yellow')
  })

  it('knows the color of oranges', () => {
    const res = sut.colorOf('orange')
    expect(res).toBe('orange')
  })
})
