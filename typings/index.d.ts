import { Fruit } from 'abs/fruit';
export declare const colorOf: (fruit: Fruit) => string;
