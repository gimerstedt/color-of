import { Fruit } from 'abs/fruit';
export declare const isBanana: (fruit: Fruit) => boolean;
export declare const isOrange: (fruit: Fruit) => boolean;
